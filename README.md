# BDCraft for Enigmatica 6

This is an UNOFFICIAL resource pack using Sphax BDCraft textures. I have included releases for various mods for the 1.15/1.16 versions, re-compiled pre-1.15 sources into the latest format, re-used similar textures from other mods, and scripted a few textures in myself.

Any texture pack or mod authors who find their work here and don't want it to be, let me know immediately.
